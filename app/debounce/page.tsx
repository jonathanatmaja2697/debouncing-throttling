"use client";

import { Button, Input, Loading, Table } from "@nextui-org/react";

import { useForm } from "react-hook-form";
import { useMovieListHooks } from "../hooks/useMovieList";

const Halo = () => {
  const { debouncedSearch, list, nonDebouncedSearch, onReset, loading } =
    useMovieListHooks();

  const { register } = useForm<{
    debounced: string;
    nonDebounced: string;
  }>({ defaultValues: { debounced: "", nonDebounced: "" } });

  const Title = ({ value }: { value: string }) => {
    return <h2 style={{ color: "black" }}>{value}</h2>;
  };

  const Text = ({ children }: { children: string }) => {
    return <span style={{ color: "gray" }}>{children}</span>;
  };

  const InputText = ({
    onChange,
    placeholder,
    inputRegister,
  }: {
    onChange: (val: any) => void;
    placeholder: string;
    inputRegister: "debounced" | "nonDebounced";
  }) => {
    return (
      <Input
        type="text"
        style={{ padding: ".4em" }}
        placeholder={placeholder}
        id="input"
        {...register(inputRegister, { onChange })}
      />
    );
  };

  const Gap = () => {
    return <div style={{ marginTop: "1em" }} />;
  };

  const MovieList = () => {
    return (
      <Table
        aria-label="Movie list table"
        css={{
          display: "inline-table",
          maxWidth: "90vw",
          minWidth: "90vw",
        }}
        bordered
      >
        <Table.Header>
          <Table.Column>No</Table.Column>
          <Table.Column>Name</Table.Column>
        </Table.Header>
        <Table.Body>
          {list.map((listItem: any, index) => {
            return (
              <Table.Row key={index}>
                <Table.Cell>{index + 1}</Table.Cell>
                <Table.Cell css={{ whiteSpace: "break-spaces" }}>
                  {listItem?.titleText?.text}
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
    );
  };

  return (
    <main
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        padding: "2rem",
        minHeight: "100vh",
        backgroundColor: "white",
      }}
    >
      <Title value="Cinema 21" />
      <Gap />
      <div style={{ display: "flex", flexDirection: "row", columnGap: ".5em" }}>
        <InputText
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            nonDebouncedSearch(e.target.value);
          }}
          placeholder="Not Debounced"
          inputRegister="nonDebounced"
        />
        <Gap />
        <InputText
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            debouncedSearch(e.target.value);
          }}
          placeholder="Debounced"
          inputRegister="debounced"
        />
      </div>
      <Gap />
      {loading ? (
        <Loading />
      ) : list.length > 0 ? (
        <div>
          <MovieList />
          <Gap />
          <Button
            size="md"
            color="error"
            style={{ width: "100%" }}
            onClick={onReset}
          >
            Reset
          </Button>
        </div>
      ) : (
        <Text>-- No result --</Text>
      )}
    </main>
  );
};

export default Halo;
