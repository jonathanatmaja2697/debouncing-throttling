"use client";

import { Button, Loading, Table } from "@nextui-org/react";

import { useCrypto } from "../hooks/useCrypto";

const Halo = () => {
  const { cryptoList, throttledSearch, loading, getCryptoList } = useCrypto();

  const Title = ({ value }: { value: string }) => {
    return <h2 style={{ color: "black" }}>{value}</h2>;
  };

  const Text = ({ children }: { children: string }) => {
    return <span style={{ color: "gray" }}>{children}</span>;
  };

  const Gap = () => {
    return <div style={{ marginTop: "1em" }} />;
  };

  const CryptoList = () => {
    return (
      <Table
        aria-label="Movie list table"
        css={{
          display: "inline-table",
          maxWidth: "90vw",
          minWidth: "90vw",
        }}
        bordered
      >
        <Table.Header>
          <Table.Column>No</Table.Column>
          <Table.Column>Crypto Name</Table.Column>
          <Table.Column>Crypto Price / IDR</Table.Column>
        </Table.Header>
        <Table.Body>
          {Object.values(cryptoList.tickers).map((c, i) => {
            return (
              <Table.Row key={i}>
                <Table.Cell>{i + 1}</Table.Cell>
                <Table.Cell>{c.name}</Table.Cell>
                <Table.Cell>
                  {`Rp` + parseInt(c.last).toLocaleString()}
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
    );
  };

  return (
    <main
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        padding: "2rem",
        minHeight: "100vh",
        backgroundColor: "white",
      }}
    >
      <Title value="Crypto Exchange" />
      <Gap />
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          columnGap: "1rem",
        }}
      >
        <Button
          css={{ minWidth: 100 }}
          color="error"
          onPress={() => getCryptoList()}
        >
          Default
        </Button>
        <Button css={{ minWidth: 100 }} onPress={() => throttledSearch()}>
          Throttled
        </Button>
      </div>
      <Gap />
      {loading ? (
        <Loading />
      ) : (
        <div>
          <Gap />
          <CryptoList />
        </div>
      )}
    </main>
  );
};

export default Halo;
