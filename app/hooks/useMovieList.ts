import axios, { AxiosRequestConfig } from "axios";

import { debounce } from "lodash";
import { useState } from "react";

export const useMovieListHooks = () => {
  const [list, setList] = useState([]);
  const [loading, setLoading] = useState(false);

  const debouncedSearch = debounce(async (title: string) => {
    setLoading(true);
    const response = await getMovieByName(title);

    setList(response.results);
  }, 500);

  const nonDebouncedSearch = async (title: string) => {
    const response = await getMovieByName(title);
    setList(response.results);
  };
  const getMovieByName = async (title: string) => {
    try {
      if (title !== "") {
        const url = `https://moviesdatabase.p.rapidapi.com/titles/search/title/${title}`;
        const options: AxiosRequestConfig = {
          method: "GET",
          url,
          params: {
            titleType: "movie",
          },
          headers: {
            "X-RapidAPI-Key":
              "aa12838a76mshbe265278780ab7dp1ed778jsn812469694d88",
            "X-RapidAPI-Host": "moviesdatabase.p.rapidapi.com",
          },
        };

        const response = await axios.request(options);

        return response.data;
      }
    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  };
  const onReset = () => {
    setList([]);
  };

  return {
    list,
    debouncedSearch,
    nonDebouncedSearch,
    onReset,
    loading,
  };
};
