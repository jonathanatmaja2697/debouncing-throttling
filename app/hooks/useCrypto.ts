import axios, { AxiosRequestConfig } from "axios";
import { useMemo, useState } from "react";

import { throttle } from "lodash";

interface CryptoListInterface {
  tickers: {
    [key: string]: {
      high: string;
      low: string;
      vol_btc: string;
      vol_idr: string;
      last: string;
      buy: string;
      sell: string;
      server_time: number;
      name: string;
    };
  };
}

export const useCrypto = () => {
  const [cryptoList, setCryptoList] = useState<CryptoListInterface>({
    tickers: {},
  });

  const [loading, setLoading] = useState(false);
  const [apiCalled, setApiCalled] = useState(0);

  const throttledSearch = useMemo(
    () =>
      throttle(
        () => {
          getCryptoList();
          setApiCalled(apiCalled + 1);
        },
        1000,
        {
          leading: true,
          trailing: true,
        }
      ),
    []
  );

  const getCryptoList = async () => {
    try {
      setLoading(true);
      const axiosOptions: AxiosRequestConfig = {
        baseURL: "https://indodax.com",
        url: `/api/summaries`,
        method: "GET",
      };
      const response = await axios.request(axiosOptions);
      setCryptoList(response.data);
      setApiCalled(apiCalled + 1);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  return {
    throttledSearch,
    setCryptoList,
    cryptoList,
    loading,
    setLoading,
    getCryptoList,
    apiCalled,
    setApiCalled,
  };
};
